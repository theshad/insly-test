<?php
define('DIR_APPLICATION', dirname(__DIR__));

// include autoload
require_once __DIR__ . '/../vendor/autoload.php';
use app\core\Application;
use app\controllers\BaseController;
use app\controllers\CalculatorController;

// Start application
$app = new Application();

$app->router->get('/', [BaseController::class, 'home']);
$app->router->post('/calculator', [CalculatorController::class, 'index']);

$app->run();