$(document).ready(function () {
    // Estimated value of the car slider
    var costSlider = $("#cost-slider").slider({
        value: 100,
        min: 100,
        max: 100000,
        slide: function(event, ui) {
            $('input[name="cost"]').val(ui.value);
        }
    });

    // Tax percentage slider
    var taxSlider = $("#tax-slider").slider({
        value: 10,
        min: 0,
        max: 100,
        slide: function(event, ui) {
            $('input[name="tax"]').val(ui.value);
        }
    });

    // Number of instalments slider
    var instalmentsSlider = $("#instalments-slider").slider({
        value: 1,
        min: 1,
        max: 12,
        slide: function(event, ui) {
            $('input[name="instalments"]').val(ui.value);
        }
    });

    $('input[name="cost"]').off('change').on('change', function() {
        costSlider.slider( "value", $(this).val());
    });
    $('input[name="tax"]').off('change').on('change', function() {
        taxSlider.slider( "value", $(this).val());
    });
    $('input[name="instalments"]').off('change').on('change', function() {
        instalmentsSlider.slider( "value", $(this).val());
    });

    // inputmask
    $('input[name="cost"]').inputmask('integer', {
        min: 100,
        max: 100000
    });
    $('input[name="tax"]').inputmask('integer', {
        min: 0,
        max:100
    });
    $('input[name="instalments"]').inputmask('integer', {
        min: 1,
        max: 12
    });

    // Activate calculate button
    $('#calculate').off('click').on('click', function (e) {
        e.preventDefault();
        var cost = $('input[name="cost"]').val();
        var tax = $('input[name="tax"]').val();
        var instalments = $('input[name="instalments"]').val();
        $.ajax({
            method: "POST",
            url: "/calculator",
            data: {
                cost: cost,
                tax: tax,
                instalments: instalments,
                time: (Date.now() / 1000)
            },
            success: function (response) {
                var data = JSON.parse(response);
                $('.main-columns').html('');
                $('.main-columns').append(paymentTemplate(data.total, $('input[name="cost"]').val()));
                $.each(data.instalments, function (k, v) {
                    $('.main-columns').append(paymentTemplate(v, '', k));
                });
                $('.payment .column.first .base span').text(data.total.base_percent);
                $('.payment .column.first .tax span').text(tax);
                $('.payment').show();
            }
        });
    });

    // Template for table with payment data
    function paymentTemplate(data, baseValue, number) {
        var title = '';
        if (number != null) {
            title = number + ' instalment';
        } else {
            title = 'Policy';
        }
        return template = '<div class="column">' +
                '<div>' + title + '</div>' +
                '<div>' + baseValue + '</div>' +
                '<div>' + data.base + '</div>' +
                '<div>' + data.commission + '</div>' +
                '<div>' + data.tax + '</div>' +
                '<div>' + data.total + '</div>' +
            '</div>';
    }
});