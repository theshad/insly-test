<?php

namespace app\controllers;

use app\core\Controller;
use app\core\Request;

class CalculatorController extends Controller {

    private int $base_price_percent = 11;
    private int $commission = 17;

    /**
     * @description Main action for calculator with json response
     */
    public function index(Request $request) {
        if ($request->getAction() == 'post') {
            // Simple data validation
            $data = self::validate($request->getData());
            // Get total data
            $totals = self::getTotal($data['cost'], $data['tax'], $data['instalments']);
            // Get additional instalments data
            $instalments = self::getInstalments($totals, $data['instalments']);

            return json_encode(['total' => $totals, 'instalments' => $instalments]);
        }
    }


    /**
     * @Description: Base price of policy, default percent is 11% from entered car value, except every Friday 15-20 o’clock (user time) when it is 13%
     */
    private function getBasePrice($cost) {
        return number_format($getBasePrice = ($cost / 100) * $this->base_price_percent, 2, '.', '');
    }

    /**
     * @Description: add commission to base price
     */
    private function getCommission($basePrice) {
        return number_format(($basePrice / 100) * $this->commission, 2, '.', '');
    }

    /**
     * @Description: add tax to base price
     */
    private function getTax($basePrice, $tax) {
        return number_format(($basePrice / 100) * $tax, 2, '.', '');
    }

    /**
     * @Description: get totals
     */
    private function getTotal($cost, $tax, $time) {
        $basePrice = self::getBasePrice($cost, $time);
        $commission = self::getCommission($basePrice);
        $tax = self::getTax($basePrice, $tax);
        $total = $basePrice + $commission + $tax;
        return ['base' => $basePrice, 'commission' => $commission, 'tax' => $tax, 'total' => round($total,2), 'base_percent' => $this->base_price_percent];
    }

    /**
     * @Description: get instalment payments
     */
    private function getInstalments($total, $count) {
        $instalments = [];
        if ($count > 1) {
            // get divided number for each position
            $data = [
                'base' => self::divideNumber($total['base'], $count),
                'commission' => self::divideNumber($total['commission'], $count),
                'tax' => self::divideNumber($total['tax'], $count),
                'total' => self::divideNumber($total['total'], $count)
            ];

            // map data into one array
            for ($i = 1; $i <= $count; $i++) {
                $instalments[$i] = [
                    'base' => number_format($data['base'][$i], 2, '.', ''),
                    'commission' => number_format($data['commission'][$i], 2, '.', ''),
                    'tax' => number_format($data['tax'][$i], 2, '.', ''),
                    'total' => number_format($data['total'][$i], 2, '.', '')
                ];
            }
        }
        return $instalments;
    }

    /**
     * @Description: function to divide payments for number of parts
     */
    private function divideNumber($number, $count) {
        $splitTotals = array_fill(1, $count, round($number / $count, 2));
        $checkTotal = $splitTotals[1] * $count;
        $difference = abs(round($checkTotal - $number, 2));
        $i = $count;
        while ($difference > 0) {
            $i = $i > 0 ? $i : $count;
            $oldDifference = $difference;
            $difference -= $difference;
            $splitTotals[$i--] += $checkTotal < $number ? $oldDifference : -$oldDifference;
        }

        return $splitTotals;
    }

    /**
     * @Description: Simple validation for POST values
     */
    private function validate($data) {
        // Validate car price
        $validate['cost'] = (int)$data['cost'];
        if ($validate['cost'] < 100) {
            $validate['cost'] = 100;
        } else if ($validate['cost'] > 100000) {
            $validate['cost'] = 100000;
        }

        // Validate Tax percentage
        $validate['tax'] = (int)$data['tax'];
        if ($validate['tax'] < 0) {
            $validate['tax'] = 0;
        } else if ($validate['tax'] > 100) {
            $validate['tax'] = 0;
        }

        // Number of instalments
        $validate['instalments'] = (int)$data['instalments'];
        if ($validate['instalments'] < 1) {
            $validate['instalments'] = 1;
        } else if ($validate['instalments'] > 12) {
            $validate['instalments'] = 12;
        }

        // Check user time to detect correct percent
        if (date('w', $data['time']) == 5 && (date('H', $data['time']) >= 15 && date('H', $data['time']) <= 20)) {
            $this->base_price_percent = 13;
        }

        return $validate;
    }
}