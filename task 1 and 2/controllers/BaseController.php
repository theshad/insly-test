<?php

namespace app\controllers;

use app\core\Controller;

/**
 * Class BaseController
 * @package app\controllers
 */
class BaseController extends Controller {

    public function home() {
        // Task 1
        $name = ['Juri', 'Tishko'];
        $oneLineName = '';
        foreach ($name as $k => $v) {
            $oneLineName .= $v . ' ';
        }

        return $this->render('home', [
            'name' => $oneLineName,
        ]);
    }
}