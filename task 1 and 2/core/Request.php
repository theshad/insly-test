<?php

namespace app\core;

/**
 * Class Request
 * @package app\core
 */
class Request {
    /**
     * @return bool|mixed|string
     * @description Parse URL and get
     */
    public function getURL() {
        $url = $_SERVER['REQUEST_URI'] ?? '/';
        $getParams = strpos($url, '?');
        if ($getParams !== false) {
            $url = substr($url, 0, $getParams);
        }
        return $url;
    }

    /**
     * @return string
     * @description Get action from URL
     */
    public function getAction() {
        return strtolower($_SERVER['REQUEST_METHOD']);
    }

    /**
     * @return bool
     * @description Check for GET request
     */
    public function isGet() {
        return $this->getAction() === 'get';
    }

    /**
     * @return bool
     * @description Check for POST request
     */
    public function isPost() {
        return $this->getAction() === 'post';
    }

    /**
     * @return array
     * @description Sanitize data
     */
    public function getData() {
        $data = [];
        if ($this->isGet()) {
            foreach ($_GET as $key => $value) {
                $data[$key] = filter_input(INPUT_GET, $key, FILTER_SANITIZE_SPECIAL_CHARS);
            }
        }
        if ($this->isPost()) {
            foreach ($_POST as $key => $value) {
                $data[$key] = filter_input(INPUT_POST, $key, FILTER_SANITIZE_SPECIAL_CHARS);
            }
        }
        return $data;
    }

}