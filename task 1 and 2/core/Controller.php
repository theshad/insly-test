<?php

namespace app\core;
/**
 * Class Controller
 * @package app\core
 */
class Controller {
    public string $layout = 'base';
    public string $action = '';

    public function render($view, $params = []) {
        return Application::$app->router->renderView($view, $params);
    }
}