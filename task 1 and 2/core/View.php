<?php

namespace app\core;

/**
 * Class View
 * @package app\core
 */
class View {
    /**
     * @param $view
     * @param array $params
     * @description Render base template and insert view template
     * @return mixed
     */
    public function renderView($view, array $params) {
        $layoutName = Application::$app->layout;
        if (Application::$app->controller) {
            $layoutName = Application::$app->controller->layout;
        }
        $viewContent = $this->renderViewOnly($view, $params);
        ob_start();
        include_once DIR_APPLICATION . '/views/layout/' . $layoutName . '.php';
        $layoutContent = ob_get_clean();
        // Replace {{content}} to content from view
        return str_replace('{{content}}', $viewContent, $layoutContent);
    }

    /**
     * @param $view
     * @param array $params
     * @description Render only template for specified view
     * @return false|string
     */
    public function renderViewOnly($view, array $params) {
        foreach ($params as $key => $value) {
            $$key = $value;
        }
        ob_start();
        include_once DIR_APPLICATION . '/views/' . $view . '.php';
        return ob_get_clean();
    }
}