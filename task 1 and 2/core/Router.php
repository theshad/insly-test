<?php

namespace app\core;

/**
 * Class Router
 * @package app\core
 */
class Router {
    public Request $request;
    protected array $routes = [];

    /**
     * Router constructor.
     * @param Request $request
     */
    public function __construct(Request $request) {
        $this->request = $request;
    }

    /**
     * @param $url
     * @param $callback
     * @description Parse GET daa
     */
    public function get($url, $callback) {
        $this->routes['get'][$url] = $callback;
    }

    /**
     * @param $url
     * @param $callback
     * @description Parse post data
     */
    public function post($url, $callback) {
        $this->routes['post'][$url] = $callback;
    }

    /**
     * @return mixed|string
     * @description Parse url and prepare data for render view
     */
    public function resolve() {
        $path = $this->request->getURL();
        $action = $this->request->getAction();
        $callback = $this->routes[$action][$path] ?? false;
        // Check for callback
        if ($callback !== false) {
            if (is_string($callback)) {
                return $this->renderView($callback);
            }
            if (is_array($callback)) {
                $controller = new $callback[0];
                $controller->action = $callback[1];
                Application::$app->controller = $controller;
                $callback[0] = $controller;
            }
            return call_user_func($callback, $this->request);
        } else {
            // if page is not exist return 404 error page
            return $this->renderView('404');
        }
    }

    /**
     * @param $view
     * @param array $params
     * @description render view
     * @return mixed
     */
    public function renderView($view, $params = []) {
        return Application::$app->view->renderView($view, $params);
    }
}