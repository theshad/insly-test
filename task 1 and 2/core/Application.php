<?php

namespace app\core;

/**
 * Class Application
 * @package app\core
 */
class Application {

    public static Application $app;
    public string $layout = 'base';
    public Router $router;
    public Request $request;
    public View $view;

    public function __construct() {
        self::$app = $this;
        $this->request = new Request();
        $this->router = new Router($this->request);
        $this->view = new View();
    }

    public function run() {
        echo $this->router->resolve();
    }
}