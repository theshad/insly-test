<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <link rel="stylesheet" href="plugins/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="plugins/jqueryui/css/jquery-ui.min.css">
        <link href="assets/css/styles.css" rel="stylesheet">
        <title>Insly - Calculator</title>
    </head>
    <body>
        <!-- Load header template -->
        <?php include_once('header.php'); ?>

        <!-- Load main content -->
        {{content}}

        <!-- Load footer teamplte -->
        <?php include_once('footer.php'); ?>
    </body>
</html>
