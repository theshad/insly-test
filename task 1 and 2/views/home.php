<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="task-1">
                <?php echo 'Author: ' . $params['name']; ?>
            </div>
        </div>
        <div class="col-12 col-sm-6 calculator">
            <div class="row row-content">
                <div class="col-sm-6 col-title">Estimated value of the car</div>
                <div class="col-sm-6 col-content">
                    <input type="text" class="form-control" name="cost" value="100" /> &euro;
                </div>
                <div class="col-12 row-slider">
                    <div id="cost-slider"></div>
                    <div class="row">
                        <div class="col-sm-6 min-value-text">100 &euro;</div>
                        <div class="col-sm-6 max-value-text">100 000 &euro;</div>
                    </div>
                </div>
            </div>
            <div class="row row-content">
                <div class="col-sm-6 col-title">Tax percentage</div>
                <div class="col-sm-6 col-content">
                    <input type="text" class="form-control" name="tax" value="10" /> %
                </div>
                <div class="col-12 row-slider">
                    <div id="tax-slider"></div>
                    <div class="row">
                        <div class="col-sm-6 min-value-text">0%</div>
                        <div class="col-sm-6 max-value-text">100%</div>
                    </div>
                </div>
            </div>
            <div class="row row-content">
                <div class="col-sm-6 col-title">Number of instalments</div>
                <div class="col-sm-6 col-content">
                    <input type="text" class="form-control" name="instalments" value="1" aria-valuemax="12" aria-valuemin="1" />
                </div>
                <div class="col-12 row-slider">
                    <div id="instalments-slider"></div>
                    <div class="row">
                        <div class="col-sm-6 min-value-text">1</div>
                        <div class="col-sm-6 max-value-text">12</div>
                    </div>
                </div>
            </div>
            <div class="btn-group">
                <button id="calculate" class="btn btn-default btn-primary">Calculate</button>
            </div>
        </div>
        <div class="col-12 payment">
            <div class="row">
                <div class="column first">
                    <div></div>
                    <div>Value</div>
                    <div class="base">Base premium (<span>11</span>%)</div>
                    <div>Commission (<span>17</span>%)</div>
                    <div class="tax">Tax (<span>10</span>%)</div>
                    <div>Total cost</div>
                </div>

                <div class="main-columns">
                </div>
            </div>
        </div>
    </div>
</div>